import asyncio
import datetime as dt
import logging

from feedme import Feed
from zoneinfo import ZoneInfo

from gdtbot.clients.baseballbot import GameThread, Subreddit
from gdtbot.clients.mlb import statsapi
from gdtbot.events.baseballbot import GameThreadPostedEvent, PostGameThreadPostedEvent
from gdtbot.events.game import GameStartEvent
from gdtbot.events.hotdog.event import (
    hotdog_game_handler,
    hotdog_post_game_handler,
    hotdog_reply_handler,
    hotdog_unsticky_game_comment_handler,
)
from gdtbot.events.reply import comment_reply_emitter

logging.basicConfig(level=logging.DEBUG, force=True)


_tz_la = ZoneInfo("America/Los_Angeles")


game_thread = GameThread(
    id=1,
    postAt=dt.datetime.now(_tz_la) - dt.timedelta(hours=1),
    startsAt=dt.datetime.now(_tz_la) + dt.timedelta(minutes=5),
    status="Posted",
    title="Test Game Chat",
    postId="121wjb9",
    gamePk=718959,
    preGamePostId=None,
    postGamePostId="121wjb9",
    createdAt=dt.datetime.now(_tz_la) - dt.timedelta(hours=12),
    updatedAt=dt.datetime.now(_tz_la),
    subreddit=Subreddit(id=1, name="troxellophilus", teamId=119),
)

game_thread_posted_event = GameThreadPostedEvent(game_thread)

post_game_thread_posted_event = PostGameThreadPostedEvent(game_thread)


async def main():
    await hotdog_game_handler(game_thread_posted_event)

    feed = Feed("hotdog_test")
    feed.add_publisher(comment_reply_emitter)
    feed.add_subscriber(hotdog_reply_handler)
    task = asyncio.create_task(feed.run())

    print(f"created hotdog game. predictions accepted until {game_thread.startsAt}")
    input("press [enter] to stop accepting predictions")
    await asyncio.sleep(30)
    try:
        task.cancel()
        await task
    except asyncio.CancelledError:
        pass

    assert game_thread.gamePk is not None
    mlb_game = await statsapi.game(game_thread.gamePk)
    assert mlb_game is not None
    game_start_event = GameStartEvent(mlb_game)
    await hotdog_unsticky_game_comment_handler(game_start_event)

    await hotdog_post_game_handler(post_game_thread_posted_event)


if __name__ == "__main__":
    asyncio.run(main())
