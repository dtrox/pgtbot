import asyncio

from gdtbot.clients.wikipedia import Wikipedia


async def run():
    data = await Wikipedia().subcategories("Los_Angeles_Dodgers")
    print(data)


asyncio.run(run())
