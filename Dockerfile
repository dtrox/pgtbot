FROM python:3.11-slim as python-base

ENV PYTHONUNBUFFERED=true

RUN apt-get update && apt-get install -y git \
    && rm -rf /var/lib/apt/lists/*

ENV POETRY_VERSION=1.8.2
ENV POETRY_HOME=/opt/pypoetry
ENV POETRY_VENV=/opt/pypoetry-venv
ENV POETRY_CACHE_DIR=/opt/cache/pypoetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true

FROM python-base as poetry-base

RUN python3 -m venv ${POETRY_VENV} \
    && ${POETRY_VENV}/bin/python -m pip install -U pip \
    && ${POETRY_VENV}/bin/python -m pip install poetry==${POETRY_VERSION}

FROM python-base as gdtbot

COPY --from=poetry-base ${POETRY_VENV} ${POETRY_VENV}

ENV PATH="${POETRY_VENV}/bin:${PATH}"

WORKDIR /gdtbot

COPY poetry.lock pyproject.toml ./

RUN poetry check
RUN poetry install --no-interaction --no-ansi --no-cache --without dev --without analysis --no-root -vvv

COPY gdtbot ./gdtbot
RUN poetry install --no-interaction --no-ansi --no-cache --only-root

ENTRYPOINT [ "/gdtbot/.venv/bin/python", "-OO", "-u", "-m", "gdtbot" ]
