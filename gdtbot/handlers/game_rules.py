"""Handlers to trigger automod rules during live games."""

import asyncpraw
import httpx
from asyncpraw.models import Comment, Submission
from asyncpraw.models.reddit.comment import CommentModeration
from asyncpraw.models.reddit.subreddit import SubredditWiki
from asyncpraw.models.reddit.wikipage import WikiPage

from gdtbot import util
from gdtbot.clients.baseballbot import baseballbot
from gdtbot.events.game import GamePostEndEvent, GameStartEvent
from gdtbot.lib import automod


async def enable_post_restriction_handler(event: GameStartEvent):
    for config in util.configs:
        if not config.live_game_post_restriction.enabled:
            continue

        if config.mlb_team_id not in (
            event.game.teams.away.id,
            event.game.teams.home.id,
        ):
            continue

        async with asyncpraw.Reddit(config.praw_bot) as reddit:
            subreddit = await reddit.subreddit(config.subreddit)
            wiki: SubredditWiki = subreddit.wiki
            page: WikiPage = await wiki.get_page("config/automoderator")

            new_automod_config = automod.enable("GAME_DURATION", page.content_md)
            await page.edit(
                content=new_automod_config,
                reason="GDT_BOT: Enabled GAME_DURATION rules.",
            )

            if config.slack_webhook is not None:
                httpx.post(
                    config.slack_webhook,
                    json={
                        "text": f"<https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}> GAME_DURATION <https://reddit.com/r/{config.subreddit}/about/wiki/config/automoderator|automod rules> have been *enabled*. Cause: <https://www.mlb.com/gameday/{event.game.gamePk}|Game Start>",
                    },
                )

            game_thread = (
                await baseballbot.recent_subreddit_game_threads(config.subreddit)
            )[0]
            submission: Submission = await reddit.submission(game_thread.postId)
            comment = await submission.reply(
                "Play ball! To facilitate discussion in this thread, only highlight posts will be permitted until after the end of the game."
            )
            assert isinstance(comment, Comment)
            mod: CommentModeration = comment.mod
            await mod.distinguish()


async def handle_disable_post_restriction(event: GamePostEndEvent):
    for config in util.configs:
        if not config.live_game_post_restriction.enabled:
            continue

        if config.mlb_team_id not in (
            event.game.teams.away.id,
            event.game.teams.home.id,
        ):
            continue

        async with asyncpraw.Reddit(config.praw_bot) as reddit:
            subreddit = await reddit.subreddit(config.subreddit)
            wiki: SubredditWiki = subreddit.wiki
            page: WikiPage = await wiki.get_page("config/automoderator")

            new_automod_config = automod.disable("GAME_DURATION", page.content_md)
            await page.edit(
                content=new_automod_config,
                reason="GDT_BOT: Disabled GAME_DURATION rules.",
            )

            if config.slack_webhook is not None:
                httpx.post(
                    config.slack_webhook,
                    json={
                        "text": f"<https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}> GAME_DURATION <https://reddit.com/r/{config.subreddit}/about/wiki/config/automoderator|automod rules> have been *disabled*. Cause: <https://www.mlb.com/gameday/{event.game.gamePk}|Game Post End>",
                    },
                )

            game_thread = (
                await baseballbot.recent_subreddit_game_threads(config.subreddit)
            )[0]
            if game_thread.postGamePostId:
                submission: Submission = await reddit.submission(
                    game_thread.postGamePostId
                )
                comment = await submission.reply(
                    "Good game, everyone. New submissions are no longer being hidden. If your submission has not been approved and otherwise follows the sub rules, feel free to resubmit or [message the mods](https://www.reddit.com/message/compose?to=/r/Dodgers) if you have any questions."
                )
                assert isinstance(comment, Comment)
                mod: CommentModeration = comment.mod
                await mod.distinguish()
