"""Handlers to flair non-gdtbot threads."""

import asyncpraw
import httpx
from asyncpraw.models import Submission
from asyncpraw.models.reddit.submission import SubmissionModeration

from gdtbot import util
from gdtbot.events.baseballbot import GameThreadPostedEvent, PostGameThreadPostedEvent


async def flair_game_thread_handler(event: GameThreadPostedEvent):
    config = util.config(event.game_thread.subreddit.name)

    if not (config.game_thread.enabled and config.game_thread.flair_id):
        return

    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        submission: Submission = await reddit.submission(event.game_thread.postId)
        mod: SubmissionModeration = submission.mod
        await mod.flair(text="Game Chat", flair_template_id=config.game_thread.flair_id)

        if config.slack_webhook is not None:
            httpx.post(
                config.slack_webhook,
                json={
                    "text": f"Flair has been set on the <https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}> <https://reddit.com/{event.game_thread.postId}|game thread>.",
                },
            )


async def flair_post_game_thread_handler(event: PostGameThreadPostedEvent):
    config = util.config(event.game_thread.subreddit.name)

    if not (config.post_game_thread.enabled and config.post_game_thread.flair_id):
        return

    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        submission: Submission = await reddit.submission(
            event.game_thread.postGamePostId
        )
        mod: SubmissionModeration = submission.mod
        await mod.flair(text="PGT", flair_template_id=config.post_game_thread.flair_id)

        if config.slack_webhook is not None:
            httpx.post(
                config.slack_webhook,
                json={
                    "text": f"Flair has been set on the <https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}> <https://reddit.com/{event.game_thread.postGamePostId}|post game thread>.",
                },
            )
