import asyncio
import logging

import asyncpraw
import asyncprawcore
import httpx
from asyncpraw.models import Submission
from asyncpraw.models.reddit.submission import SubmissionModeration

from gdtbot import util
from gdtbot.events.clock import MorningEvent, NightEvent
from gdtbot.lib import daily_thread

logger = logging.getLogger(__name__)


async def daily_thread_handler(event: MorningEvent):
    config = util.config(event.subreddit)

    content = await daily_thread.get_submission_content(
        event.date, config.subreddit, config.mlb_team_id
    )

    logger.info("submitting daily thread for %s", f"r/{config.subreddit}")

    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        subreddit = await reddit.subreddit(config.subreddit)
        submission = await subreddit.submit(
            title=content.title,
            selftext=content.selftext,
            flair_id=config.daily_thread.flair_id,
            flair_text="Daily Chat" if config.daily_thread.flair_id else None,
        )
        mod: SubmissionModeration = submission.mod
        await mod.sticky(state=True)
        await submission.load()

    if config.slack_webhook is not None:
        httpx.post(
            config.slack_webhook,
            json={
                "text": f"Posted <https://reddit.com/{submission.id}|daily thread> to <https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}>.",
            },
        )


async def unsticky_daily_threads_handler(event: NightEvent):
    config = util.config(event.subreddit)

    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        subreddit = await reddit.subreddit(config.subreddit)
        stickies: list[Submission] = []

        logger.info("finding stickied threads for %s", f"r/{config.subreddit}")

        # capture all stickies by checking sticky num until we get a NotFound
        sticky_num = 1  # stickies start at 1
        while True:
            try:
                stickies.append(await subreddit.sticky(number=sticky_num))
                await asyncio.sleep(2)
            except asyncprawcore.NotFound:
                break
            sticky_num += 1
            if sticky_num >= 5:
                # too many stickies
                break

        logger.info("unstickying daily threads for %s", f"r/{config.subreddit}")

        for submission in stickies:
            await submission.load()
            mod: SubmissionModeration = submission.mod

            if submission.title.lower().startswith("postgame"):
                await mod.sticky(state=False)

                if config.slack_webhook is not None:
                    httpx.post(
                        config.slack_webhook,
                        json={
                            "text": f"<https://reddit.com/{submission.id}|Postgame thread> has been unstickied on <https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}>.",
                        },
                    )

            if submission.title.lower().startswith("daily chat"):
                await mod.sticky(state=False)

                if config.slack_webhook is not None:
                    httpx.post(
                        config.slack_webhook,
                        json={
                            "text": f"<https://reddit.com/{submission.id}|Game Day thread> has been unstickied on <https://reddit.com/r/{config.subreddit}|r/{config.subreddit.title()}>.",
                        },
                    )
