import datetime
import logging
import re
from collections import defaultdict

import asyncpraw
from asyncpraw.models import Comment, Submission
from asyncpraw.models.reddit.comment import CommentModeration
from jinja2 import Environment, PackageLoader

from gdtbot import util
from gdtbot.clients.mlb import statsapi
from gdtbot.clients.savant import baseballsavant
from gdtbot.events.baseballbot import GameThreadPostedEvent, PostGameThreadPostedEvent
from gdtbot.events.game import GameStartEvent
from gdtbot.events.reply import CommentReplyEvent
from gdtbot.lib.hotdog import db
from gdtbot.lib.hotdog.error import HotdogCommentError, HotdogPlayParseError
from gdtbot.lib.hotdog.model import Game, Play

RECENT_LEADERBOARD_LENGTH = 5
SEASON_LEADERBOARD_LENGTH = 5
GAME_LEADERBOARD_LENGTH = 10


logger = logging.getLogger(__name__)


jinja_env = Environment(loader=PackageLoader("gdtbot"))


async def hotdog_game_handler(event: GameThreadPostedEvent):
    """Start a hot dog game."""
    if not event.game_thread.gamePk:
        logger.warning("game_thread has no gamePk, hotdog game not posted")
        return

    season = event.game_thread.startsAt.year
    subreddit = event.game_thread.subreddit.name

    recent_leaderboard = await db.get_recent_leaderboard(season, subreddit)
    recent_leaderboard = [x for x in recent_leaderboard if x.wins > 0]

    season_leaderboard = await db.get_season_leaderboard(season, subreddit)
    season_leaderboard = [x for x in season_leaderboard if x.wins > 0]

    tpl = jinja_env.get_template("hotdog_game_comment.md")
    body = tpl.render(
        recent_leaderboard=recent_leaderboard[:RECENT_LEADERBOARD_LENGTH],
        season_leaderboard=season_leaderboard[:SEASON_LEADERBOARD_LENGTH],
    )

    config = util.config(subreddit)
    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        submission: Submission = await reddit.submission(event.game_thread.postId)
        comment: Comment = await submission.reply(body)  # type: ignore
        if comment is None:
            raise HotdogCommentError("unable to post hotdog game comment")
        try:
            mod: CommentModeration = comment.mod
            await mod.distinguish(sticky=True)
        except Exception:
            logger.exception("unable to mod distinguish hotdog game comment")

    game = Game(
        subreddit=subreddit,
        game_pk=event.game_thread.gamePk,
        comment_id=comment.id,
        season=season,
        deadline=event.game_thread.startsAt,
    )
    await db.write_game(game)


_play_pat = re.compile(r"![Pp]\w* ([\w ]+) (\d+)")


async def _validate_hotdog_play(body: str, team_name: str):
    mat = _play_pat.search(body)
    if mat is None:
        raise HotdogPlayParseError("couldn't match play pattern")
    players = await baseballsavant.player_search(mat.group(1))
    players = [p for p in players if p["mlb"] and p["is_player"]]
    if not players:
        raise HotdogPlayParseError("couldn't find any mlb players for search")
    team_players = [p for p in players if p["name_display_club"] == team_name]
    try:
        if team_players:
            player = team_players[0]
        else:
            player = players[0]
        player_id = int(player["id"])
        player_name = str(player["name"])
        prediction = int(mat.group(2).lstrip("0"))
    except (TypeError, ValueError):
        raise HotdogPlayParseError("couldn't parse values as integer")
    return player_id, prediction, player_name


async def hotdog_reply_handler(event: CommentReplyEvent):
    """Record plays in a hotdog game."""
    game = await db.get_game(comment_id=event.parent_id[3:])
    if game is None:
        logger.info(
            f"could not find a hotdog game associated with comment {event.parent_id[3:]}"
        )
        return

    config = util.config(event.subreddit)
    team = await statsapi.team(config.mlb_team_id)

    logger.debug(f"handling hotdog game reply {event.comment_id} author {event.author}")
    created_at = datetime.datetime.fromtimestamp(
        event.created_utc, tz=datetime.timezone.utc
    )
    if event.edited:
        edited = datetime.datetime.fromtimestamp(event.edited, tz=datetime.timezone.utc)
    else:
        edited = None

    if (edited or created_at) >= game.deadline:
        mlb_game = await statsapi.game(game.game_pk)
        if mlb_game and mlb_game.status.startswith("P"):
            # allow predictions during delayed starts
            logger.info(
                f"game is in delayed start, allowing comment {event.comment_id} posted after deadline"
            )
        else:
            logger.info(f"comment {event.comment_id} posted after deadline")
            return

    try:
        (
            player_id,
            prediction,
            player_name,
        ) = await _validate_hotdog_play(event.body, team.teamName)
    except HotdogPlayParseError as err:
        logger.info(
            f"unable to parse a play from comment {event.comment_id}, reason: {str(err)}"
        )
        return

    play = Play(
        username=event.author,
        subreddit=game.subreddit,
        game_pk=game.game_pk,
        player_id=player_id,
        prediction=prediction,
    )
    await db.write_play(play)
    logger.info(f"accepted play for comment {event.comment_id}")

    season_hotdogs = (
        await db.get_season_hotdogs(game.deadline.year, event.subreddit, event.author)
        or 0
    )

    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        comment: Comment = await reddit.comment(event.comment_id, fetch=False)
        tpl = jinja_env.get_template("hotdog_play_reply.md")
        body = tpl.render(
            player=player_name, prediction=prediction, hotdogs=season_hotdogs
        )
        await comment.reply(body)
    logger.info(f"replied to play comment {event.comment_id}")


_stats_pat = re.compile(r"![Hh](?:\w+)?(?:\s+(\d{4}))?")


async def hotdog_stats_handler(event: CommentReplyEvent):
    """Provide a user with their stats on request."""
    # validate the reply as a request for hotdog stats in a supported subreddit
    mat = _stats_pat.search(event.body)
    if not mat:
        return

    if mat.group(1):
        season = int(mat.group(1))
    else:
        season = datetime.datetime.now().year

    try:
        config = util.config(event.subreddit)
    except KeyError:
        return

    # query for the user's hotdog stats
    season_leaderboard = await db.get_season_leaderboard(season, config.subreddit)
    user_stats = next(
        (row for row in season_leaderboard if row.username == event.author), None
    )

    if user_stats:
        tpl = jinja_env.get_template("hotdog_stats_reply.md")
        reply_body = tpl.render(stats=user_stats)
    else:
        reply_body = f"You have no 🌭 prediction attempts for the {season} season."

    # reply to the user
    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        comment: Comment = await reddit.comment(event.comment_id, fetch=False)
        await comment.reply(reply_body)
    logger.info(f"replied to stats comment {event.comment_id}")


async def hotdog_unsticky_game_comment_handler(event: GameStartEvent):
    """Unsticky the game comment after game start."""
    for config in util.configs:
        if config.mlb_team_id not in (
            event.game.teams.away.id,
            event.game.teams.home.id,
        ):
            continue

        game = await db.get_game(game_pk=event.game.gamePk)
        if game is None:
            logger.debug(f"no hotdog game found for game_pk {event.game.gamePk}")
            continue

        try:
            async with asyncpraw.Reddit(config.praw_bot) as reddit:
                game_comment: Comment = await reddit.comment(
                    game.comment_id, fetch=False
                )
                mod: CommentModeration = game_comment.mod
                await mod.distinguish(sticky=False)
                logger.info(f"unstickied hotdog game comment {game.comment_id}")
        except Exception:
            logger.exception(
                f"unable to mod distinguish hotdog game comment {game.comment_id} in subreddit {game.subreddit}"
            )


_event_type_tb = {
    "single": 1,
    "double": 2,
    "triple": 3,
    "home_run": 4,
}


async def hotdog_post_game_handler(event: PostGameThreadPostedEvent):
    """Record successes and submit post game comment."""
    if not event.game_thread.gamePk:
        logger.warning("game_thread has no gamePk, hotdog post game not posted")
        return

    game_pk = event.game_thread.gamePk
    season = event.game_thread.startsAt.year
    subreddit = event.game_thread.subreddit.name
    post_game_post_id = event.game_thread.postGamePostId
    if post_game_post_id is None:
        raise ValueError("unable to identify post game thread")

    # mark successes
    gumbo = await statsapi.gumbo(game_pk)
    tb = defaultdict(int)
    for play in gumbo.liveData.plays.allPlays:
        tb[play.matchup.batter.id] += _event_type_tb.get(play.result.eventType, 0)
    predictions = await db.get_predictions(game_pk, subreddit)
    for play in predictions:
        play.success = tb.get(play.player_id, 0) >= play.prediction
        await db.update_success(play)

    # post the pgt comment
    season_lb = await db.get_season_leaderboard(season, subreddit)
    season_lb_lk = {r.username: r for r in season_lb}
    game_lb = await db.get_game_leaderboard(game_pk, subreddit)
    leaderboards = [
        (
            game,
            season_lb_lk[game.username],
            (
                await statsapi.get(
                    f"/api/v1/people/{game.player_id}", fields="people,fullName"
                )
            )["people"][0]["fullName"],
        )
        for game in game_lb
        if game.username in season_lb_lk  # should always be true
        and game.hotdogs  # only include winners
    ]
    tpl = jinja_env.get_template("hotdog_postgame_comment.md")
    body = tpl.render(leaderboards=leaderboards[:GAME_LEADERBOARD_LENGTH])

    config = util.config(subreddit)
    async with asyncpraw.Reddit(config.praw_bot) as reddit:
        submission: Submission = await reddit.submission(post_game_post_id)
        comment: Comment = await submission.reply(body)  # type: ignore
        if comment is None:
            raise HotdogCommentError("unable to post hotdog post game comment")
        try:
            mod: CommentModeration = comment.mod
            await mod.distinguish()
        except Exception:
            logger.exception("unable to mod distinguish hotdog post game comment")
