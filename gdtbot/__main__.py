import asyncio
import logging

from feedme import Feed

from gdtbot import util
from gdtbot.events import baseballbot, clock, game, reply
from gdtbot.handlers import daily_thread, flair, game_rules, hotdog
from gdtbot.lib import hotdog as hotdog_lib


async def main():
    configs = util.configure()

    feed = Feed("gdtbot")

    print("=== GDTBot ===")

    print(f"FeedMe Database: {feed.db.filepath}")
    print(f"Subreddits: {', '.join(c.subreddit for c in configs)}")

    hotdog_lib.init()
    print(f"Hotdog Database: {hotdog_lib.DBPATH}")

    feed.add_publisher(clock.morning_emitter, delay=30)
    feed.add_publisher(clock.night_emitter, delay=30)
    feed.add_publisher(game.game_emitter, delay=120)
    feed.add_publisher(reply.comment_reply_emitter, delay=60)
    feed.add_publisher(baseballbot.baseballbot_emitter, delay=120)

    feed.add_subscriber(daily_thread.daily_thread_handler)
    feed.add_subscriber(daily_thread.unsticky_daily_threads_handler)

    feed.add_subscriber(hotdog.hotdog_game_handler)
    feed.add_subscriber(hotdog.hotdog_stats_handler)
    feed.add_subscriber(hotdog.hotdog_reply_handler)
    feed.add_subscriber(hotdog.hotdog_post_game_handler)
    feed.add_subscriber(hotdog.hotdog_unsticky_game_comment_handler)

    feed.add_subscriber(flair.flair_game_thread_handler)
    feed.add_subscriber(flair.flair_post_game_thread_handler)

    feed.add_subscriber(game_rules.handle_disable_post_restriction)
    feed.add_subscriber(game_rules.enable_post_restriction_handler)

    await feed.run()


if __name__ == "__main__":
    from gdtbot.logging import SlackFormatter, SlackHandler

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(name)s : %(levelname)s : %(message)s")
    )
    root_logger.addHandler(stream_handler)
    slack_handler = SlackHandler()
    slack_handler.setLevel(logging.ERROR)
    slack_handler.setFormatter(SlackFormatter())
    root_logger.addHandler(slack_handler)

    try:
        asyncio.run(main())
    except Exception:
        logger = logging.getLogger(__name__)
        logger.fatal("Fatal error occurred.", exc_info=True)
        raise
