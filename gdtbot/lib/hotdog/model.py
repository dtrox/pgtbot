import datetime
from dataclasses import dataclass


@dataclass
class Game:
    subreddit: str
    game_pk: int
    comment_id: str
    season: int
    deadline: datetime.datetime


@dataclass
class Play:
    username: str
    subreddit: str
    game_pk: int
    player_id: int
    prediction: int
    success: bool | None = None


@dataclass
class SeasonLeaderboardRow:
    season: int
    subreddit: str
    username: str
    wins: int
    hotdogs: int
    rank: int
    win_pct: float
    hotdogs_per_win: float


@dataclass
class GameLeaderboardRow:
    subreddit: str
    game_pk: int
    username: str
    player_id: int
    prediction: int
    hotdogs: int
    rank: int
