class HotdogError(Exception):
    pass


class HotdogPlayParseError(HotdogError):
    pass


class HotdogCommentError(HotdogError):
    pass


class HotdogStatsParseError(HotdogError):
    pass
