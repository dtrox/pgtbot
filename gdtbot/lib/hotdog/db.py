import asyncio
import dataclasses
import datetime as dt
import logging
import re
import sqlite3
import typing
from collections import abc
from contextlib import contextmanager
from dataclasses import asdict
from pathlib import Path

from gdtbot.lib.hotdog import queries
from gdtbot.lib.hotdog.model import (
    Game,
    GameLeaderboardRow,
    Play,
    SeasonLeaderboardRow,
)

logger = logging.getLogger(__name__)


_dbpath = Path.home() / ".cache" / "gdtbot" / "hotdog.db"
_dbpath.parent.mkdir(mode=0o755, parents=True, exist_ok=True)


def serialize_dataclass_record(obj):
    return {
        k: v.isoformat() if isinstance(v, (dt.datetime, dt.date)) else v
        for k, v in asdict(obj).items()
    }


class DataclassInstance(typing.Protocol):
    __dataclass_fields__: typing.ClassVar[dict]
    __init__: typing.Callable[..., None]


_C = typing.TypeVar("_C", bound=DataclassInstance)


def deserialize_dataclass_record(
    obj: abc.Sequence[typing.Any], cls: typing.Type[_C]
) -> _C:
    def gen():
        for idx, field in enumerate(dataclasses.fields(cls)):
            if field.type == dt.datetime:
                val = dt.datetime.fromisoformat(obj[idx])
            elif field.type == dt.date:
                val = dt.date.fromisoformat(obj[idx])
            elif isinstance(field.type, type) and not isinstance(obj[idx], field.type):
                val = field.type(obj[idx])
            else:
                val = obj[idx]
            yield val

    return cls(*gen())


@contextmanager
def connect():
    con = sqlite3.connect(_dbpath)
    try:
        with con:
            yield con
    finally:
        con.close()


def query(query_text: str, **params: typing.Any) -> list[abc.Sequence[typing.Any]]:
    with connect() as con:
        cur = con.execute(query_text, params)
        records = list(cur)
    return records


async def aquery(query_text: str, **params: typing.Any):
    return await asyncio.to_thread(query, query_text, **params)


def init():
    with connect() as con:
        con.executescript(queries.INITDB)


async def write_game(game: Game):
    await aquery(queries.INSERT_GAME, **serialize_dataclass_record(game))


async def write_play(play: Play):
    await aquery(queries.INSERT_PLAY, **serialize_dataclass_record(play))


_markdown_fmtr_pat = re.compile(r"([_*])")


def _escape_markdown(value: str) -> str:
    return _markdown_fmtr_pat.sub(r"\\\\\1", value)


async def get_recent_leaderboard(season: int, subreddit: str):
    rows = await aquery(queries.RECENT_LEADERBOARD, season=season, subreddit=subreddit)

    leaderboard = [SeasonLeaderboardRow(*r) for r in rows]
    for row in leaderboard:
        row.username = _escape_markdown(row.username)

    return leaderboard


async def get_season_leaderboard(season: int, subreddit: str):
    rows = await aquery(queries.SEASON_LEADERBOARD, season=season, subreddit=subreddit)

    leaderboard = [SeasonLeaderboardRow(*r) for r in rows]
    for row in leaderboard:
        row.username = _escape_markdown(row.username)

    return leaderboard


async def get_game_leaderboard(game_pk: int, subreddit: str):
    rows = await aquery(queries.GAME_LEADERBOARD, game_pk=game_pk, subreddit=subreddit)

    leaderboard = [GameLeaderboardRow(*r) for r in rows]
    for row in leaderboard:
        row.username = _escape_markdown(row.username)

    return leaderboard


async def get_predictions(game_pk: int, subreddit: str):
    rows = await aquery(queries.PREDICTIONS, game_pk=game_pk, subreddit=subreddit)
    return [Play(*r) for r in rows]


async def update_success(play: Play):
    await aquery(queries.MARK_SUCCESS, **serialize_dataclass_record(play))


async def get_game(*, comment_id: str | None = None, game_pk: int | None = None):
    if comment_id:
        col = "comment_id"
        val = comment_id
    elif game_pk:
        col = "game_pk"
        val = game_pk
    else:
        raise ValueError("missing param")
    rows = await aquery(f"select * from game where {col} = :{col}", **{col: val})
    if not rows:
        return None
    return deserialize_dataclass_record(rows[0], Game)


async def get_season_hotdogs(season: int, subreddit: str, username: str) -> int | None:
    rows = await aquery(
        queries.READ_SEASON_HOTDOGS,
        season=season,
        subreddit=subreddit,
        username=username,
    )
    if not rows:
        return None
    return rows[0][0]
