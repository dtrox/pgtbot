"""Hotdog prediction game library."""

from gdtbot.lib.hotdog.db import _dbpath
from gdtbot.lib.hotdog.db import init as init

DBPATH = str(_dbpath.absolute())
