INITDB = """
create table if not exists game (
    subreddit text,
    game_pk int,
    comment_id text,
    season int,
    deadline datetime,
    primary key (subreddit, game_pk)
)
;

create table if not exists play (
    username text,
    subreddit text,
    game_pk int,
    player_id int,
    prediction int,
    success bool,
    primary key (username, subreddit, game_pk)
)
;
"""


INSERT_GAME = """
insert or replace into game values
(
    :subreddit,
    :game_pk,
    :comment_id,
    :season,
    :deadline
)
;
"""


INSERT_PLAY = """
insert or replace into play values
(
    :username,
    :subreddit,
    :game_pk,
    :player_id,
    :prediction,
    :success
)
;
"""


RECENT_LEADERBOARD = """
with recent_total as (
    select
        season,
        subreddit,
        username,
        count(*) as plays,
        count(*) filter (where success) as wins,
        sum(prediction) filter (where success) as hotdogs
    from play
    join (
        select
            season,
            subreddit,
            game_pk,
            row_number() over (partition by subreddit order by deadline desc) as gb
        from game
    ) using (subreddit, game_pk)
    where
        season = :season
        and subreddit = :subreddit
        and gb <= 10
    group by
        1, 2, 3
)
select
    season,
    subreddit,
    username,
    wins,
    coalesce(hotdogs, 0) as hotdogs,
    rank() over (partition by season, subreddit order by hotdogs desc) as rank,
    wins / cast(plays as float) as win_pct,
    coalesce(hotdogs, 0) / cast(wins as float) as hotdogs_per_win
from recent_total
order by
    hotdogs desc,
    username
;
"""


SEASON_LEADERBOARD = """
with season_total as (
    select
        season,
        subreddit,
        username,
        count(*) as plays,
        count(*) filter (where success) as wins,
        sum(prediction) filter (where success) as hotdogs
    from play
    join game using (subreddit, game_pk)
    where
        season = :season
        and subreddit = :subreddit
    group by
        1, 2, 3
)
select
    season,
    subreddit,
    username,
    wins,
    coalesce(hotdogs, 0) as hotdogs,
    rank() over (partition by season, subreddit order by hotdogs desc) as rank,
    wins / cast(plays as float) as win_pct,
    coalesce(hotdogs, 0) / cast(wins as float) as hotdogs_per_win
from season_total
order by
    hotdogs desc,
    username
;
"""


GAME_LEADERBOARD = """
with game_total as (
    select
        subreddit,
        game_pk,
        username,
        player_id,
        prediction,
        case when success then prediction else 0 end as hotdogs
    from play
    where
        subreddit = :subreddit
        and game_pk = :game_pk
)
select
    subreddit,
    game_pk,
    username,
    player_id,
    prediction,
    hotdogs,
    rank() over (partition by subreddit, game_pk order by hotdogs desc) as rank
from game_total
order by
    hotdogs desc,
    username
;
"""


PREDICTIONS = """
select *
from play
where
    game_pk = :game_pk
    and subreddit = :subreddit
;
"""


MARK_SUCCESS = """
update play set
    success = :success
where
    username = :username
    and game_pk = :game_pk
    and subreddit = :subreddit
;
"""


READ_SEASON_HOTDOGS = """
select
    sum(prediction) filter (where success) as hotdogs
from play
join game using (subreddit, game_pk)
where
    season = :season
    and subreddit = :subreddit
    and username = :username
"""
