"""Functions for retrieving and formatting daily thread content."""

import asyncio
import datetime
import logging
import random
import urllib.parse
from functools import lru_cache
from pathlib import Path
from typing import Optional

from httpx import HTTPStatusError
from jinja2 import Environment, PackageLoader
from pydantic import BaseModel
from zoneinfo import ZoneInfo

from gdtbot.clients import mlb
from gdtbot.clients.mlb import statsapi
from gdtbot.clients.wikipedia import wikipedia
from gdtbot.util import config

logger = logging.getLogger(__name__)

jinja_env = Environment(loader=PackageLoader("gdtbot"))


@lru_cache()
def _read_discussion_questions():
    path = Path(__file__).parent.absolute() / "data" / "questions.txt"
    questions = tuple(path.read_text(encoding="utf-8").splitlines())
    return questions


tbd_pitcher = mlb.Player(
    primaryPosition=mlb.Position(
        code="1",
        name="Starter",
        type="Pitcher",
        abbreviation="SP",
    )
)


empty_pitching_stats = mlb.PitchingStats()


async def get_throwback(date: datetime.date, mlb_team_id: int) -> Optional[str]:
    tpl = jinja_env.get_template("throwback.md")

    wpgames = [
        g
        for d in (date.replace(year=date.year - (x + 1)) for x in range(5))
        for g in await statsapi.games(d, mlb_team_id)
    ]

    wpplays: list[tuple[mlb.Game, mlb.PlayWinProbability, float]] = []
    for wpgame in wpgames:
        try:
            wpplays.extend(
                (
                    wpgame,
                    p,
                    (-1 if wpgame.teams.away.id == mlb_team_id else 1)
                    * p.homeTeamWinProbabilityAdded,
                )
                for p in await statsapi.win_probability(wpgame.gamePk)
                if p.pitch_events() and p.result.description
            )
        except HTTPStatusError:
            continue

    if wpplays:
        wpgame, wpplay, _ = random.choice(
            sorted(wpplays, key=lambda t: t[2], reverse=True)[:3]
        )
    else:
        wpgame = wpplay = None

    if wpgame and wpplay:
        logger.info(
            "found at-bat %s from game %s for throwback, wpa %s",
            wpplay.atBatIndex,
            wpgame.gamePk,
            wpplay.homeTeamWinProbabilityAdded,
        )
        if wpplay.about.halfInning == "top":
            pre_away_score = wpplay.result.awayScore - wpplay.result.rbi
            pre_home_score = wpplay.result.homeScore
        else:
            pre_away_score = wpplay.result.awayScore
            pre_home_score = wpplay.result.homeScore - wpplay.result.rbi
        return tpl.render(
            wpgame=wpgame,
            wpplay=wpplay,
            pre_away_score=pre_away_score,
            pre_home_score=pre_home_score,
            home_away="away" if wpgame.teams.away.id == mlb_team_id else "home",
        )


def get_questions():
    tpl = jinja_env.get_template("questions.md")
    return tpl.render(questions=random.sample(_read_discussion_questions(), 3))


async def get_important_dates():
    season = await statsapi.season_dates()
    events = [
        f"{x} - *{v.strftime(r'%a %d %B %Y')}*"
        for x, v in season.important_dates()
        if v > datetime.datetime.now(tz=ZoneInfo("America/New_York")).date()
    ]
    tpl = jinja_env.get_template("important_dates.md")
    return tpl.render(important_dates=events[:3])


async def get_wikipedia_article(team_full_name: str):
    team_category = team_full_name.replace(" ", "_")
    categories = await wikipedia.subcategories(team_category)
    await asyncio.sleep(0.5)
    category = random.choice(
        [c for c in (team_category, *categories) if not c.endswith("templates")]
    )
    wiki_article_url, wiki_article_title = await wikipedia.random_in_category(category)
    parts = urllib.parse.urlparse(wiki_article_url)
    url = urllib.parse.urlunparse(
        (*parts[:3], urllib.parse.quote(parts.params), *parts[4:])
    )
    return url, wiki_article_title


class SubmissionContent(BaseModel):
    title: str
    selftext: str


async def get_game_day_submission_content(
    date: datetime.date, subreddit: str, mlb_team_id: int
) -> SubmissionContent:
    games = await statsapi.games(date, mlb_team_id)

    game_tpl = jinja_env.get_template("game.md")

    day_type = await statsapi.day_type(date)

    rendered_games = []
    for game in games:
        away_batting_leaders = await statsapi.team_batting_leaders(game.teams.away.id)
        home_batting_leaders = await statsapi.team_batting_leaders(game.teams.home.id)

        batting_leaders = list(
            enumerate(zip(away_batting_leaders[:3], home_batting_leaders[:3]))
        )

        if game.teams.away.probablePitcher:
            pitcher, _, stats = await statsapi.player(
                game.teams.away.probablePitcher, stats_game_type=day_type
            )
            away_probable = (pitcher, stats or empty_pitching_stats)
        else:
            away_probable = (tbd_pitcher, empty_pitching_stats)

        if game.teams.home.probablePitcher:
            pitcher, _, stats = await statsapi.player(
                game.teams.home.probablePitcher, stats_game_type=day_type
            )
            home_probable = (pitcher, stats or empty_pitching_stats)
        else:
            home_probable = (tbd_pitcher, empty_pitching_stats)

        rendered = game_tpl.render(
            mlb_team_id=mlb_team_id,
            game=game,
            timezone=config(subreddit).timezone,
            away_team=game.teams.away,
            away_team_batting=await statsapi.team_batting_stats(
                game.teams.away.id, game_type=day_type
            ),
            away_probable=away_probable,
            home_team=game.teams.home,
            home_team_batting=await statsapi.team_batting_stats(
                game.teams.home.id, game_type=day_type
            ),
            home_probable=home_probable,
            batting_leaders=batting_leaders,
        )
        rendered_games.append(rendered)

    cfg = config(subreddit)
    team = await statsapi.team(cfg.mlb_team_id)
    wiki_article_url, wiki_article_title = await get_wikipedia_article(team.name)

    gdt_tpl = jinja_env.get_template("game_day_thread.md")

    try:
        throwback = await get_throwback(date, mlb_team_id) or ""
    except Exception:
        logger.exception("error occurred while getting throwback")
        throwback = ""

    try:
        important_dates = await get_important_dates()
    except Exception:
        logger.exception("error occurred while getting important dates")
        important_dates = ""

    selftext = gdt_tpl.render(
        team_website_url=f"https://www.mlb.com/{team.teamName.lower()}",
        games=rendered_games,
        wiki_article_title=wiki_article_title,
        wiki_article_url=wiki_article_url,
        important_dates=important_dates,
        throwback=throwback,
        questions=get_questions(),
        subreddit_name=subreddit,
    )

    return SubmissionContent(
        title=f"Daily Chat {date.strftime('%-m/%-d')} ⚾ Game Day",
        selftext=selftext,
    )


async def get_off_day_submission_content(
    date: datetime.date, subreddit: str, mlb_team_id: int
) -> SubmissionContent:
    upcoming_schedule = await statsapi.upcoming_games(date, mlb_team_id, num_days=3)

    probables = []
    for game in upcoming_schedule:
        if game.teams.away.probablePitcher:
            away_prob = (await statsapi.player(game.teams.away.probablePitcher))[0]
        else:
            away_prob = tbd_pitcher
        if game.teams.home.probablePitcher:
            home_prob = (await statsapi.player(game.teams.home.probablePitcher))[0]
        else:
            home_prob = tbd_pitcher
        probables.append((away_prob, home_prob))

    cfg = config(subreddit)
    team = await statsapi.team(cfg.mlb_team_id)
    wiki_article_url, wiki_article_title = await get_wikipedia_article(team.name)

    odt_tpl = jinja_env.get_template("off_day_thread.md")

    try:
        throwback = await get_throwback(date, mlb_team_id) or ""
    except Exception:
        logger.exception("error occurred while getting throwback")
        throwback = ""

    try:
        important_dates = await get_important_dates()
    except Exception:
        logger.exception("error occurred while getting mlb important dates")
        important_dates = ""

    selftext = odt_tpl.render(
        team_website_url=f"https://www.mlb.com/{team.teamName.lower()}",
        upcoming_schedule=zip(upcoming_schedule, probables),
        important_dates=important_dates,
        wiki_article_title=wiki_article_title,
        wiki_article_url=wiki_article_url,
        throwback=throwback,
        questions=get_questions(),
        subreddit_name=subreddit,
        timezone=config(subreddit).timezone,
    )

    return SubmissionContent(
        title=f"Daily Chat {date.strftime('%-m/%-d')} ⚾ Off Day",
        selftext=selftext,
    )


async def get_offseason_submission_content(
    date: datetime.date, subreddit: str, mlb_team_id: int
) -> SubmissionContent:
    cfg = config(subreddit)

    if date.month > 10 or date.month == 10 and statsapi.day_type(date) == "O":
        next_season = date.year + 1
    else:
        next_season = date.year

    world_series = await statsapi.world_series(next_season - 1)
    spring_training = await statsapi.spring_training(next_season, cfg.mlb_team_id)
    team = await statsapi.team(cfg.mlb_team_id)

    days_since_ws = (
        date - world_series[-1].gameDate.astimezone(cfg.timezone).date()
    ).days
    days_until_st = (
        spring_training[0].gameDate.astimezone(cfg.timezone).date() - date
    ).days

    wiki_article_url, wiki_article_title = await get_wikipedia_article(team.name)

    tpl = jinja_env.get_template("offseason_day_thread.md")

    try:
        important_dates = await get_important_dates()
    except Exception:
        logger.exception("error occurred while getting mlb important dates")
        important_dates = ""

    selftext = tpl.render(
        team_website_url=f"https://www.mlb.com/{team.teamName.lower()}",
        days_since_ws=days_since_ws,
        days_until_st=days_until_st,
        team_name=(await statsapi.team(mlb_team_id)).teamName,
        next_season=next_season,
        wiki_article_title=wiki_article_title,
        wiki_article_url=wiki_article_url,
        important_dates=important_dates,
        questions=get_questions(),
        subreddit_name=subreddit,
    )

    return SubmissionContent(
        title=f"Daily Chat {date.strftime('%-m/%-d')} ⚾ Offseason",
        selftext=selftext,
    )


async def get_submission_content(date: datetime.date, subreddit: str, mlb_team_id: int):
    day_type = await statsapi.day_type(date)

    if day_type == "O":
        return await get_offseason_submission_content(date, subreddit, mlb_team_id)
    elif day_type in ("S", "R", "P"):
        games = await statsapi.games(date, mlb_team_id)
        if games:
            return await get_game_day_submission_content(date, subreddit, mlb_team_id)
        else:
            return await get_off_day_submission_content(date, subreddit, mlb_team_id)
    else:
        raise ValueError(f"Unrecognized day_type: {day_type}")
