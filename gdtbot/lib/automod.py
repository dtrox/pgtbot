import re

from ruamel.yaml import YAML
from ruamel.yaml.parser import ParserError as YamlParserError
from ruamel.yaml.scanner import ScannerError as YamlScannerError

_RULE_GROUP_REGEX_TEMPLATE = r"(# GDT_BOT: BEGIN {rule_group} RULES\r?\n)([\S\s\n]*)(# GDT_BOT: END {rule_group} RULES)"


class AutomodError(Exception):
    pass


class AutomodConfigInvalidError(AutomodError):
    pass


def _validate_yaml(text: str):
    try:
        _ = list(YAML(typ="safe").load_all(text))
    except (YamlParserError, YamlScannerError):
        raise AutomodConfigInvalidError("AutomodConfig is invalid YAML.")


def _uncomment_repl(match: re.Match) -> str:
    rules = "".join(
        line.lstrip("#") for line in match.group(2).splitlines(keepends=True)
    )
    return f"{match.group(1)}{rules}{match.group(3)}"


def enable(rule_group: str, automod_config: str) -> str:
    pattern = re.compile(
        _RULE_GROUP_REGEX_TEMPLATE.format(rule_group=rule_group), flags=re.MULTILINE
    )
    new_automod_config = pattern.sub(_uncomment_repl, automod_config)
    _validate_yaml(new_automod_config)
    return new_automod_config


def _comment_repl(match: re.Match) -> str:
    rules = "".join(f"#{line}" for line in match.group(2).splitlines(keepends=True))
    return f"{match.group(1)}{rules}{match.group(3)}"


def disable(rule_group: str, automod_config: str) -> str:
    pattern = re.compile(
        _RULE_GROUP_REGEX_TEMPLATE.format(rule_group=rule_group), flags=re.MULTILINE
    )
    new_automod_config = pattern.sub(_comment_repl, automod_config)
    _validate_yaml(new_automod_config)
    return new_automod_config
