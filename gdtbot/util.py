import dataclasses
import functools
from pathlib import Path

from ruamel.yaml import YAML
from zoneinfo import ZoneInfo

yaml = YAML(typ="safe")


@dataclasses.dataclass
class Feature(object):
    enabled: bool = False


@dataclasses.dataclass
class Thread(Feature):
    flair_id: str | None = None


@dataclasses.dataclass
class Game(Feature):
    currency: str = "hot dog"
    currency_plural: str = "hot dogs"
    currency_symbol: str = "🌭"


@dataclasses.dataclass
class LiveGamePostRestriction(Feature):
    unrestrict_delay: int = 0


@dataclasses.dataclass
class Config(object):
    subreddit: str
    praw_bot: str
    mlb_team_id: int
    timezone: ZoneInfo
    slack_webhook: str | None = None
    daily_thread: Thread = dataclasses.field(default_factory=Thread)
    live_game_post_restriction: LiveGamePostRestriction = dataclasses.field(
        default_factory=LiveGamePostRestriction
    )
    game_thread: Thread = dataclasses.field(default_factory=Thread)
    post_game_thread: Thread = dataclasses.field(default_factory=Thread)


def _default_config_path() -> Path:
    path = Path.cwd() / ".gdtbot" / "config.yaml"
    if path.exists():
        return path

    path = Path.home() / ".gdtbot" / "config.yaml"
    if path.exists():
        return path

    path = Path.cwd() / ".pgtbot" / "config.yaml"
    if path.exists():
        return path

    path = Path.home() / ".pgtbot" / "config.yaml"
    if path.exists():
        return path

    raise ValueError(
        "No config found. Either place a config in `.gdtbot/config.yaml` or specify a path with `--config`."
    )


@functools.lru_cache()
def configure() -> list[Config]:
    config = _default_config_path()

    configs = []
    subreddits = yaml.load(Path(config).read_text())["subreddits"]
    for name, cfg in subreddits.items():
        config = Config(
            subreddit=name,
            praw_bot=cfg["praw_bot"],
            mlb_team_id=cfg["mlb_team_id"],
            timezone=ZoneInfo(cfg["timezone"]),
            slack_webhook=cfg.get("slack_webhook"),
            daily_thread=Thread(**cfg.get("daily_thread", {})),
            live_game_post_restriction=LiveGamePostRestriction(
                **cfg.get("live_game_post_restriction", {})
            ),
            game_thread=Thread(**cfg.get("game_thread", {})),
            post_game_thread=Thread(**cfg.get("post_game_thread", {})),
        )
        configs.append(config)

    return configs


configs = configure()


@functools.lru_cache()
def config(subreddit: str) -> Config:
    return {c.subreddit: c for c in configs}[subreddit]
