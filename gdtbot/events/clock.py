"""Clock-based events."""

import datetime
import logging

from feedme import Event

from gdtbot import util

logger = logging.getLogger(__name__)


class DailyEvent(Event):
    def __init__(self, date: datetime.date, subreddit: str):
        self.date = date
        self.subreddit = subreddit

    def key(self):
        return f"{self.subreddit}_{self.date.isoformat()}"


THRESHOLD_HOUR = 5


class MorningEvent(DailyEvent):
    pass


class NightEvent(DailyEvent):
    pass


async def morning_emitter() -> list[MorningEvent]:
    events: list[MorningEvent] = []

    for config in util.configs:
        now = datetime.datetime.now(tz=config.timezone)
        if now.time() < datetime.time(THRESHOLD_HOUR):
            continue
        if config.daily_thread.enabled:
            events.append(MorningEvent(now.date(), config.subreddit))

    return events


async def night_emitter() -> list[NightEvent]:
    events: list[NightEvent] = []

    for config in util.configs:
        now = datetime.datetime.now(tz=config.timezone)
        if now.time() > datetime.time(THRESHOLD_HOUR):
            continue
        if config.daily_thread.enabled:
            events.append(NightEvent(now.date(), config.subreddit))

    return events
