from feedme import Event

from gdtbot import util
from gdtbot.clients.baseballbot import GameThread, baseballbot


class BaseballBotEvent(Event):
    def __init__(self, game_thread: GameThread):
        self.game_thread = game_thread

    def key(self):
        return str(self.game_thread.gamePk)


class GameThreadPostedEvent(BaseballBotEvent):
    pass


class PostGameThreadPostedEvent(BaseballBotEvent):
    pass


async def baseballbot_emitter() -> list[BaseballBotEvent]:
    subreddits = {c.subreddit for c in util.configs}

    events: list[BaseballBotEvent] = []

    for subreddit in subreddits:
        game_threads = await baseballbot.recent_subreddit_game_threads(subreddit)

        for game_thread in game_threads:
            if game_thread.postId is not None:
                events.append(GameThreadPostedEvent(game_thread))

            if game_thread.postGamePostId is not None:
                events.append(PostGameThreadPostedEvent(game_thread))

    return events
