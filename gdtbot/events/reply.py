import asyncio
import datetime as dt
import logging
from dataclasses import dataclass

import asyncpraw
from asyncpraw.models import Comment
from asyncprawcore.exceptions import ResponseException
from feedme import Event

from gdtbot import util

logger = logging.getLogger(__name__)


@dataclass
class CommentData:
    comment_id: str


class CommentReplyEvent(Event):
    def __init__(
        self,
        subreddit: str,
        submission_id: str,
        parent_id: str,
        comment_id: str,
        author: str,
        body: str,
        created_utc: float,
        edited: float | None,
    ):
        self.subreddit = subreddit
        self.submission_id = submission_id
        self.parent_id = parent_id
        self.comment_id = comment_id
        self.author = author
        self.body = body
        self.created_utc = created_utc
        self.edited = edited

    def key(self):
        if self.edited:
            edited = dt.datetime.fromtimestamp(self.edited, tz=dt.timezone.utc)
            return f"{self.subreddit}__{self.comment_id}__{edited.isoformat()}"
        return f"{self.subreddit}__{self.comment_id}"


def _process_reply(subreddit: str, reply: Comment):
    if reply.author is None:
        logger.debug(f"no author, ignoring reply {reply.id}")
        return None
    created_at = dt.datetime.fromtimestamp(reply.created_utc, tz=dt.timezone.utc)
    if (dt.datetime.now(tz=dt.timezone.utc) - created_at).total_seconds() > 600:
        logger.debug(f"reply is too old to consider {reply.id}")
        return None
    return CommentReplyEvent(
        subreddit,
        reply.submission.id,
        reply.parent_id,
        reply.id,
        reply.author.name,
        reply.body,
        reply.created_utc,
        getattr(reply, "edited", None),
    )


async def comment_reply_emitter() -> list[CommentReplyEvent]:
    events: list[CommentReplyEvent] = []

    for config in util.configs:
        try:
            async with asyncpraw.Reddit(config.praw_bot) as reddit:
                logger.debug("creating events from comment replies")
                async for reply in reddit.inbox.comment_replies(limit=25):
                    if event := _process_reply(config.subreddit, reply):
                        events.append(event)

                logger.debug("creating events from mentions")
                async for mention in reddit.inbox.mentions(limit=25):
                    if event := _process_reply(config.subreddit, mention):
                        events.append(event)

        except ResponseException as err:
            logger.error(err)
            logger.info("delaying next attempt")
            await asyncio.sleep(60)

    return events
