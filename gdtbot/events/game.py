"""Emit events from MLB games."""

import pendulum
from feedme import Event

from gdtbot import util
from gdtbot.clients import mlb
from gdtbot.clients.mlb import statsapi

GAME_POST_END_DELAY = 5


class GameEvent(Event):
    name: str

    def __init__(self, game: mlb.Game):
        self.game = game

    def key(self):
        return f"game__{self.name}_{self.game.gamePk}"


class GameStartEvent(GameEvent):
    name: str = "start"


class GameEndEvent(GameEvent):
    name: str = "end"


class GamePostEndEvent(GameEvent):
    name: str = "postend"


async def game_emitter() -> list[GameEvent]:
    team_ids = set(c.mlb_team_id for c in util.configs)

    events: list[GameEvent] = []

    games = await statsapi.games()

    for game in games:
        if game.teams.away.id not in team_ids and game.teams.home.id not in team_ids:
            # game is irrelevant to all configs
            continue

        if game.status.startswith("L") or game.status.startswith("I"):
            events.append(GameStartEvent(game))

        if game.status.startswith("F") or game.status.startswith("O"):
            events.append(GameEndEvent(game))

            gumbo = await statsapi.get(f"/api/v1.1/game/{game.gamePk}/feed/live")
            try:
                last_play_end_time = pendulum.parse(
                    gumbo["liveData"]["plays"]["allPlays"][-1]["about"]["endTime"]
                )
            except (KeyError, IndexError):
                try:
                    last_play_end_time = pendulum.parse(
                        gumbo["liveData"]["plays"]["allPlays"][-1]["playEndTime"]
                    )
                except (KeyError, IndexError):
                    last_play_end_time = None

            assert isinstance(last_play_end_time, pendulum.DateTime)

            if (
                last_play_end_time is not None
                and (pendulum.now() - last_play_end_time).in_minutes()
                >= GAME_POST_END_DELAY
            ):
                events.append(GamePostEndEvent(game))

    return events
