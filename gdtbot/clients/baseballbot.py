import datetime
from typing import Optional

from pydantic import BaseModel

from gdtbot.clients.base import BaseClient


class Subreddit(BaseModel):
    id: int
    name: str
    teamId: Optional[int]


class GameThread(BaseModel):
    id: int
    postAt: datetime.datetime
    startsAt: datetime.datetime
    status: str
    title: Optional[str]
    postId: Optional[str]
    gamePk: Optional[int]
    preGamePostId: Optional[str]
    postGamePostId: Optional[str]
    createdAt: datetime.datetime
    updatedAt: datetime.datetime
    subreddit: Subreddit


class BaseballBotClient(BaseClient):
    def __init__(self):
        super().__init__(base_url="https://baseballbot.io")

    async def get_subreddit_game_threads(self, subreddit: str) -> list[dict]:
        path = f"/subreddits/{subreddit.lower()}/game_threads.json"
        return (await self.get(path)).json()["data"]

    async def recent_subreddit_game_threads(self, subreddit: str) -> list[GameThread]:
        objects = await self.get_subreddit_game_threads(subreddit)
        return [GameThread.model_validate(g) for g in objects]

    async def get_game_threads(self) -> list[dict]:
        return (await self.get("/game_threads.json")).json()["data"]

    async def recent_game_threads(self) -> list[GameThread]:
        objects = await self.get_game_threads()
        return [GameThread.model_validate(g) for g in objects]


baseballbot = BaseballBotClient()
