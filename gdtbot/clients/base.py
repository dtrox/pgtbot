import asyncio
import importlib.metadata
from random import random

import httpx
from aiocache import cached


class BaseClient(object):
    def __init__(self, *, base_url: str, headers: dict | None = None):
        self._lock = asyncio.Lock()
        self._client: httpx.AsyncClient = httpx.AsyncClient(
            base_url=base_url,
            headers={
                "User-Agent": f"gdtbot/{importlib.metadata.version('gdtbot')} (+https://gitlab.com/dtrox/pgtbot)",
                "Accept": "application/json",
                **(headers or {}),
            },
        )

    async def close(self):
        await self._client.aclose()

    async def _timed_release(self):
        await asyncio.sleep(random() + 0.5)
        self._lock.release()

    @cached(ttl=60)
    async def _get(self, *args, **kwargs):
        await self._lock.acquire()
        asyncio.create_task(self._timed_release())
        response = await self._client.get(*args, **kwargs)
        response.raise_for_status()
        return response

    async def get(self, *args, **kwargs) -> httpx.Response:
        return await self._get(*args, **kwargs)
