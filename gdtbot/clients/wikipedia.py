from typing import Tuple

import bs4

from gdtbot.clients.base import BaseClient


class Wikipedia(BaseClient):
    def __init__(self):
        super().__init__(
            base_url="https://en.wikipedia.org", headers={"Accept": "text/html"}
        )

    async def subcategories(self, category: str) -> list[str]:
        url = f"/wiki/Category:{category}"
        response = await self.get(url, follow_redirects=True)
        response.raise_for_status()
        soup = bs4.BeautifulSoup(response.text, features="html.parser")
        subcat_div = soup.find("div", attrs={"id": "mw-subcategories"})
        assert isinstance(subcat_div, bs4.Tag)
        links = subcat_div.find_all("a")
        return [
            link["href"][len("/wiki/Category:") :] for link in links if "href" in link
        ]

    async def random_in_category(self, category: str) -> Tuple[str, str]:
        url = f"/wiki/Special:RandomInCategory/{category}"
        response = await self.get(url, follow_redirects=True)
        response.raise_for_status()
        title = bs4.BeautifulSoup(response.text, features="html.parser").title.text  # type: ignore
        return str(response.url), title


wikipedia = Wikipedia()


if __name__ == "__main__":
    import asyncio

    async def main():
        result = await wikipedia.subcategories("Los_Angeles_Dodgers")
        print(result)

    asyncio.run(main())
