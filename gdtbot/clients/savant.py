import asyncio

from aiocache import cached

from gdtbot.clients.base import BaseClient


class BaseballSavantClient(BaseClient):
    def __init__(self):
        super().__init__(base_url="https://baseballsavant.mlb.com")

    @cached(ttl=3600)
    async def player_search(self, search: str) -> dict:
        return (await self.get("/player/search-all", params={"search": search})).json()


baseballsavant = BaseballSavantClient()


if __name__ == "__main__":

    async def main():
        client = BaseballSavantClient()
        result = await client.player_search("mookie")
        print(result)

    asyncio.run(main())
