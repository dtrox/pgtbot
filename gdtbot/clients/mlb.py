import datetime
from typing import Optional

import dateutil.parser
import httpx
import pydantic
from aiocache import cached
from pydantic import BaseModel, field_validator

from gdtbot.clients.base import BaseClient


class Location(BaseModel):
    city: str
    state: Optional[str] = None
    stateAbbrev: Optional[str] = None
    defaultCoordinates: Optional[dict[str, int | float]] = None
    country: str


class TimeZone(BaseModel):
    id: str
    offset: int
    tz: str


class Venue(BaseModel):
    id: int
    name: str
    location: Location
    timeZone: Optional[TimeZone] = None


class PitchingStats(BaseModel):
    inningsPitched: str = "-"
    strikeoutsPer9Inn: str = "-"
    walksPer9Inn: str = "-"
    homeRunsPer9: str = "-"
    era: str = "-"


class BattingStats(BaseModel):
    gamesPlayed: int = 0
    runs: int = 0
    avg: str = "-"
    obp: str = "-"
    slg: str = "-"
    ops: str = "-"
    atBatsPerHomeRun: str = "-"


class Lookup(BaseModel):
    id: int | None = None
    name: str = "N/A"


class Team(BaseModel):
    id: int
    name: str
    abbreviation: str
    teamName: str
    locationName: str
    sport: Lookup
    league: Lookup
    division: Optional[Lookup] = None
    shortName: str
    probablePitcher: Optional[int] = None
    score: Optional[int] = None

    @field_validator("probablePitcher", mode="before")
    def _parse_probable(cls, v):
        return v["id"]


class Position(BaseModel):
    code: str = "-"
    name: str = "-"
    type: str = "-"
    abbreviation: str = "-"


class Player(BaseModel):
    id: int = -1
    fullName: str = "TBD"
    useName: str = ""
    lastName: str = ""
    primaryPosition: Position = Position()
    batSide: str = "-"
    pitchHand: str = "-"

    @field_validator("batSide", "pitchHand", mode="before")
    def _parse_hand(cls, v):
        return v["code"]


class Teams(BaseModel):
    away: Team
    home: Team

    @field_validator("away", "home", mode="before")
    def _parse_team(cls, v):
        return {**v, **v["team"]}


class Game(BaseModel):
    gamePk: int
    gameType: str
    season: int
    gameDate: datetime.datetime
    officialDate: datetime.date
    status: str
    venue: Venue
    teams: Teams
    gamesInSeries: Optional[int] = None
    seriesGameNumber: Optional[int] = None

    @field_validator("status", mode="before")
    def _get_status(cls, v):
        return v["statusCode"]


class HighlightKeyword(BaseModel):
    type: str
    value: str
    displayName: str


class HighlightPlayback(BaseModel):
    name: str
    url: str


class Highlight(BaseModel):
    type: str
    date: datetime.datetime
    id: str
    headline: str
    blurb: str
    keywordsAll: list[HighlightKeyword]
    title: str
    description: str
    duration: datetime.time
    guid: str
    playbacks: list[HighlightPlayback]

    def preferred_playback(self):
        for pb in self.playbacks:
            if pb.name == "mp4Avc":
                return pb.url
            if pb.name == "highBit":
                return pb.url
        else:
            return None

    def team_ids(self):
        return [int(k.value) for k in self.keywordsAll if k.type == "team_id"]


class PlayResult(BaseModel):
    eventType: Optional[str] = None
    description: Optional[str] = None
    rbi: int
    awayScore: int
    homeScore: int


class PlayAbout(BaseModel):
    halfInning: str
    inning: int


class Count(BaseModel):
    balls: int
    strikes: int
    outs: int


class MatchupPlayer(BaseModel):
    id: int
    fullName: str = ""


class Matchup(BaseModel):
    batter: MatchupPlayer
    pitcher: MatchupPlayer


class PlayEvent(BaseModel):
    index: int
    playId: Optional[str] = None
    preCount: Count


class PlayWinProbability(BaseModel):
    result: PlayResult
    about: PlayAbout
    matchup: Matchup
    playEvents: list[PlayEvent]
    playEndTime: datetime.datetime
    homeTeamWinProbability: float
    awayTeamWinProbability: float
    homeTeamWinProbabilityAdded: float
    atBatIndex: int

    def pitch_events(self):
        return [p for p in self.playEvents if p.playId]


class Season(BaseModel):
    seasonId: str
    hasWildcard: bool
    preSeasonStartDate: datetime.date
    preSeasonEndDate: datetime.date
    seasonStartDate: datetime.date
    springStartDate: datetime.date
    springEndDate: datetime.date
    regularSeasonStartDate: datetime.date
    lastDate1stHalf: datetime.date
    allStarDate: datetime.date
    firstDate2ndHalf: datetime.date
    regularSeasonEndDate: datetime.date
    postSeasonStartDate: datetime.date
    postSeasonEndDate: datetime.date
    seasonEndDate: datetime.date
    offseasonStartDate: datetime.date
    offSeasonEndDate: datetime.date
    seasonLevelGamedayType: str
    gameLevelGamedayType: str

    def important_dates(self):
        return (
            ("Start of Spring Training", self.springStartDate),
            ("End of Spring Training", self.springEndDate),
            ("Opening Day", self.regularSeasonStartDate),
            ("Jackie Robinson Day", datetime.date(datetime.date.today().year, 4, 15)),
            ("End of the 1st Half", self.lastDate1stHalf),
            ("All Star Game", self.allStarDate),
            ("Start of the 2nd Half", self.firstDate2ndHalf),
            ("Roberto Clemente Day", datetime.date(datetime.date.today().year, 9, 15)),
            ("End of the Regular Season", self.regularSeasonEndDate),
            ("Start of the Post Season", self.postSeasonStartDate),
            ("End of the Post Season", self.postSeasonEndDate),
            ("Start of the Offseason", self.offseasonStartDate),
        )


class GumboPlayResult(BaseModel):
    eventType: str


class GumboPlayMatchupPlayer(BaseModel):
    id: int
    fullName: str


class GumboPlayMatchup(BaseModel):
    batter: GumboPlayMatchupPlayer


class GumboPlay(BaseModel):
    result: GumboPlayResult
    matchup: GumboPlayMatchup


class GumboPlays(BaseModel):
    allPlays: list[GumboPlay]


class GumboLiveData(BaseModel):
    plays: GumboPlays


class Gumbo(BaseModel):
    liveData: GumboLiveData


def _parse_player_and_stats(
    obj: dict,
) -> tuple[Player, Optional[BattingStats], Optional[PitchingStats]]:
    player = Player.model_validate(obj)

    batting = None
    pitching = None
    if "stats" in obj:
        for stats in obj["stats"]:
            if stats["group"]["displayName"] == "hitting":
                batting = BattingStats.model_validate(stats["splits"][0]["stat"])
            elif stats["group"]["displayName"] == "pitching":
                pitching = PitchingStats.model_validate(stats["splits"][0]["stat"])

    return player, batting, pitching


class StatsAPIClient(BaseClient):
    def __init__(self):
        super().__init__(base_url="https://statsapi.mlb.com")

    async def get(self, path: str, **params) -> dict:
        return (await super().get(path, params=params)).json()

    @cached(ttl=24 * 3600)
    async def world_series(self, season: int) -> tuple[Game, ...]:
        data = await self.get(
            "/api/v1/schedule",
            startDate=f"{season}-10-10",
            endDate=f"{season}-11-15",
            sportId=1,
            gameType="W",
            hydrate="venue(location),team,probablePitcher",
        )
        return tuple(Game.model_validate(g) for d in data["dates"] for g in d["games"])

    @cached(ttl=24 * 3600)
    async def spring_training(self, season: int, team_id: int) -> tuple[Game, ...]:
        data = await self.get(
            "/api/v1/schedule",
            startDate=f"{season}-02-15",
            endDate=f"{season}-04-05",
            sportId=1,
            teamId=team_id,
            gameType="S",
            hydrate="venue(location),team,probablePitcher",
        )
        return tuple(Game.model_validate(g) for d in data["dates"] for g in d["games"])

    @cached(ttl=24 * 3600)
    async def team(self, team_id: int) -> Team:
        data = (await self.get(f"/api/v1/teams/{team_id}"))["teams"][0]
        return Team.model_validate(data)

    @cached(ttl=12 * 3600)
    async def team_batting_stats(
        self, team_id: int, game_type: str = "R"
    ) -> BattingStats:
        try:
            data = await self.get(
                f"/api/v1/teams/{team_id}/stats",
                stats="season",
                group="hitting",
                sportId="1",
                gameType=game_type,
            )
            return BattingStats.model_validate(data["stats"][0]["splits"][0]["stat"])
        except httpx.HTTPStatusError as err:
            if err.response.status_code == httpx.codes.NOT_FOUND:
                return BattingStats()
            raise

    async def game(self, game_pk: int) -> Game | None:
        params = {
            "gamePk": game_pk,
            "hydrate": "venue(location,timezone),team,probablePitcher",
        }
        data = await self.get("/api/v1/schedule", **params)
        try:
            return Game.model_validate(data["dates"][0]["games"][0])
        except (KeyError, IndexError):
            return None

    async def games(
        self, date: Optional[datetime.date] = None, team_id: Optional[int] = None
    ) -> tuple[Game, ...]:
        params = {
            "sportId": 1,
            "hydrate": "venue(location,timezone),team,probablePitcher",
        }
        if date is not None:
            params["date"] = date.isoformat()
        if team_id is not None:
            params["teamId"] = team_id

        data = await self.get("/api/v1/schedule", **params)

        return tuple(Game.model_validate(g) for d in data["dates"] for g in d["games"])

    @cached(ttl=24 * 3600)
    async def season(self, date: datetime.date) -> int:
        if date.month < 10 or date.month == 12:
            return date.year

        seasons = await self.get("/api/v1/seasons", sportId=1, season=date.year)
        end_date = dateutil.parser.isoparse(
            seasons["seasons"][0]["seasonEndDate"]
        ).date()

        if date > end_date:
            return date.year + 1
        else:
            return date.year

    @cached(ttl=12 * 3600)
    async def day_type(self, date: datetime.date) -> str:
        if date.month in (12, 1):
            return "O"
        elif 3 < date.month < 10:
            return "R"

        seasons = await self.get("/api/v1/seasons", sportId=1, season=date.year)
        season_dates = seasons["seasons"][0]

        if (
            season_dates["springStartDate"]
            <= date.isoformat()
            <= season_dates["springEndDate"]
        ):
            return "S"
        elif (
            season_dates["regularSeasonStartDate"]
            <= date.isoformat()
            <= season_dates["regularSeasonEndDate"]
        ):
            return "R"
        elif (
            season_dates["postSeasonStartDate"]
            <= date.isoformat()
            <= season_dates["postSeasonEndDate"]
        ):
            return "P"

        return "O"

    @cached(ttl=12 * 3600)
    async def upcoming_games(
        self, date: datetime.date, team_id: int, num_days=3
    ) -> tuple[Game, ...]:
        data = await self.get(
            "/api/v1/schedule",
            sportId=1,
            teamId=team_id,
            startDate=date.isoformat(),
            endDate=(date + datetime.timedelta(days=num_days)),
            hydrate="venue(location),team,probablePitcher",
        )
        return tuple(Game.model_validate(g) for d in data["dates"] for g in d["games"])

    @cached(ttl=12 * 3600)
    async def player(
        self, player_id: int, stats_game_type: str = "R"
    ) -> tuple[Player, Optional[BattingStats], Optional[PitchingStats]]:
        data = (
            await self.get(
                f"/api/v1/people/{player_id}",
                hydrate=f"stats(groups=[hitting,pitching],type=season,gameType={stats_game_type})",
            )
        )["people"][0]
        return _parse_player_and_stats(data)

    @cached(ttl=12 * 3600)
    async def team_batting_leaders(
        self, team_id: int
    ) -> list[tuple[Player, BattingStats]]:
        data = await self.get(
            "/api/v1/stats/leaders",
            statGroup="hitting",
            leaderCategories="onBasePlusSlugging",
            teamId=team_id,
            playerPool="Qualified",
            hydrate="person(stats(groups=[hitting,pitching],type=season))",
            limit=9,
        )

        batting_leaders: list[tuple[Player, BattingStats]] = []
        try:
            for leader in data["leagueLeaders"][0]["leaders"]:
                player, batting_stats, _ = _parse_player_and_stats(leader["person"])
                batting_leaders.append((player, batting_stats or BattingStats()))
        except IndexError:
            pass

        return batting_leaders

    async def highlights(self, game_pk: int) -> list[Highlight]:
        data = await self.get(f"/api/v1/game/{game_pk}/content")
        return pydantic.TypeAdapter(list[Highlight]).validate_python(
            data["highlights"]["highlights"]["items"]
        )

    async def win_probability(self, game_pk: int) -> list[PlayWinProbability]:
        data = await self.get(f"/api/v1/game/{game_pk}/winProbability")
        return pydantic.TypeAdapter(list[PlayWinProbability]).validate_python(data)

    @cached(ttl=3 * 3600)
    async def season_dates(self) -> Season:
        data = await self.get(
            "/api/v1/seasons", sportId=1, season=datetime.datetime.now().year
        )
        return Season.model_validate(data["seasons"][0])

    async def gumbo(self, game_pk: int) -> Gumbo:
        data = await self.get(f"/api/v1.1/game/{game_pk}/feed/live")
        return Gumbo.model_validate(data)


statsapi = StatsAPIClient()
