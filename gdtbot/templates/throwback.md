## _On this day in {{ wpgame.gameDate.year }}..._

**[{{ wpgame.teams.away.teamName }} @ {{ wpgame.teams.home.teamName }}](https://www.mlb.com/gameday/{{ wpgame.gamePk }}/play/{{ wpplay.atBatIndex }}), {{ wpgame.teams.away.abbreviation }} {{ pre_away_score }} {{ wpgame.teams.home.abbreviation }} {{ pre_home_score }}, {{ wpplay.about.halfInning[:3] }} {{ wpplay.about.inning }}, {{ wpplay.pitch_events()[-1].preCount.outs }} out, {{ wpplay.matchup.batter.fullName }} facing {{ wpplay.matchup.pitcher.fullName }}...**  
{{ wpplay.result.description }} [**VIDEO**](https://baseballsavant.mlb.com/sporty-videos?playId={{ wpplay.pitch_events()[-1].playId }}&videoType={{ home_away.upper() }})

_Win Probability Added: **{{ wpplay.homeTeamWinProbabilityAdded|abs|round(2) }}**_
