**🌭 Prediction Results 🌭**

Results for the top successful predictions this game.

Get your detailed 🌭 stats by mentioning or replying to this user with:
> !hotdogs  
> !h

| **Rk** | **User** | **Prediction** | 🌭 |
|-------:|:---------|:---------------|---:|
{% for game, season, player in leaderboards %}|{{ game.rank }}|{{ game.username }}|_{{ player }}_|{{ game.hotdogs }}|
{% endfor %}

^(_🌭 have no value other than bragging rights._)
