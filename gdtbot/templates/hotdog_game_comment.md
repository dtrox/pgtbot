**🌭 Prediction Game 🌭**

Predict a player's [Total Bases](https://www.mlb.com/glossary/standard-stats/total-bases) in this game. If the player earns at least that many TB, you get 🌭 equal to your prediction.

Make a prediction by replying to this comment like:
> !predict Mookie Betts 4  
> !p Outman 2

^(_**Note:** Only 1 prediction is considered per user per game. Predictions are only accepted prior to game start._)

---

***Leaderboard***

**Last 10**

| **Rank** | **User** | **Wins** | 🌭 |
|:---------|:---------|---------:|--------:|
{% for row in recent_leaderboard %}|**{{ row.rank }}**|{{ row.username }}|{{ row.wins }}|{{ row.hotdogs }}|
{% endfor %}

**Season**

| **Rank** | **User** | **Wins** | 🌭 |
|:---------|:---------|---------:|--------:|
{% for row in season_leaderboard %}|**{{ row.rank }}**|{{ row.username }}|{{ row.wins }}|{{ row.hotdogs }}|
{% endfor %}

---

Get your detailed 🌭 stats by mentioning or replying to this user with:
> !hotdogs  
> !h

^(_🌭 have no value other than bragging rights._)
