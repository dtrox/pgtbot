Good morning, r/{{ subreddit_name.title() }}!

* [__Team Website__]({{team_website_url}})
* __Wikipedia Article of the Day:__ [{{ wiki_article_title }}]({{ wiki_article_url }})

---

{% for game in games %}
{{ game }}

{% endfor %}

{% if throwback %}
---

{{ throwback }}
{% endif %}

---

{{ important_dates }}

---

{{questions}}

---

Have a great day, r/{{ subreddit_name.title() }}.
