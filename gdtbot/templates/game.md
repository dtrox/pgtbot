## ⚾ {% if away_team.id == mlb_team_id %} {{ away_team.teamName }} @ {{ home_team.teamName }} {% else %} {{ home_team.teamName }} vs. {{ away_team.teamName }} {% endif %} ⚾

First Pitch: **{{ game.gameDate.astimezone(timezone).strftime('%A, %b %-d, %Y %-I:%M %p') }}**  
{{ game.venue.name }} in {{ game.venue.location.city }}, {{ game.venue.location.state }}

### Starters

|Team|Starting Pitcher|IP|K/9|BB/9|HR/9|ERA|
|:---|:---------------|:-|:--|:---|:---|:--|
|**{{ away_team.abbreviation }}**|**{{ away_probable[0].pitchHand }}HP {{ away_probable[0].fullName }}**|{{ away_probable[1].inningsPitched }}|{{ away_probable[1].strikeoutsPer9Inn }}|{{ away_probable[1].walksPer9Inn }}|{{ away_probable[1].homeRunsPer9 }}|{{ away_probable[1].era }}|
|**{{ home_team.abbreviation }}**|**{{ home_probable[0].pitchHand }}HP {{ home_probable[0].fullName }}**|{{ home_probable[1].inningsPitched }}|{{ home_probable[1].strikeoutsPer9Inn }}|{{ home_probable[1].walksPer9Inn }}|{{ home_probable[1].homeRunsPer9 }}|{{ home_probable[1].era }}|

### Team Offense

|Team|G |AVG|OBP|SLG|OPS|AB/HR|R/G|
|:---|:-|:--|:--|:--|:--|:----|:--|
|**{{ away_team.abbreviation }}**|{{ away_team_batting.gamesPlayed }}|{{ away_team_batting.avg }}|{{ away_team_batting.obp }}|{{ away_team_batting.slg }}|{{ away_team_batting.ops }}|{{ away_team_batting.atBatsPerHomeRun }}|{{ '%0.2f' % (away_team_batting.runs / (away_team_batting.gamesPlayed or 1))|float }}|
|**{{ home_team.abbreviation }}**|{{ home_team_batting.gamesPlayed }}|{{ home_team_batting.avg }}|{{ home_team_batting.obp }}|{{ home_team_batting.slg }}|{{ home_team_batting.ops }}|{{ home_team_batting.atBatsPerHomeRun }}|{{ '%0.2f' % (home_team_batting.runs / (home_team_batting.gamesPlayed or 1))|float }}|

{% if batting_leaders %}
### Top Performers

|**{{ away_team.teamName }}**|Pos|OPS|Rank|OPS|Pos|**{{ home_team.teamName }}**|
|-------------------:|--:|--:|:--:|:--|:--|:-------------------|
{% for rank, (away, home) in batting_leaders %}|{{ away[0].fullName }}|{{ away[0].primaryPosition.abbreviation }}|{{ away[1].ops }}|{{ rank + 1 }}|{{ home[1].ops }}|{{ home[0].primaryPosition.abbreviation }}|{{ home[0].fullName }}|
{% endfor %}
{% endif %}
