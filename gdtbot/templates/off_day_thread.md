Good morning, r/{{ subreddit_name.title() }}!

* [__Team Website__]({{team_website_url}})
* __Wikipedia Article of the Day:__ [{{ wiki_article_title }}]({{ wiki_article_url }})

---

## Upcoming Games

|Date|Away|Home|Time|Away Probable|Home Probable|
|:---|:---|:---|:---|:------------|:------------|
{% for game, probable in upcoming_schedule %}|{{ game.gameDate.astimezone(timezone).strftime("%-m/%-d") }}|**{{ game.teams.away.abbreviation }}**|**{{ game.teams.home.abbreviation }}**|{{ game.gameDate.astimezone(timezone).strftime("%-I:%M %P %Z") }}|**{{ probable[0].pitchHand }}HP {{ probable[0].useName[0] }}. {{probable[0].lastName}}**|**{{ probable[1].pitchHand }}HP {{ probable[1].useName[0] }}. {{probable[1].lastName}}**|
{% endfor %}

---

{{ important_dates }}

{% if throwback %}
---

{{ throwback }}
{% endif %}

---

{{questions}}

---

Have a great day, r/{{ subreddit_name.title() }}.
