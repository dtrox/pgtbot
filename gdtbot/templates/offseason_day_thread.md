Good morning, r/{{ subreddit_name.title() }}!

Welcome to **day {{ days_since_ws }}** of the offseason. Only **{{ days_until_st }} days** until the {{ team_name }}' first Spring Training game of {{ next_season }}!

* [__Team Website__]({{team_website_url}})
* __Wikipedia Article of the Day:__ [{{ wiki_article_title }}]({{ wiki_article_url }})

---

{{ important_dates }}

---

{{questions}}

---

Have a great day, r/{{ subreddit_name.title() }}.
