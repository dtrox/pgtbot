You have **{{ stats.hotdogs }}** 🌭 for the {{ stats.season }} season.

|User|Season|Rk|Wins|🌭|Win%|🌭/W|
|:-|-:|-:|-:|-:|-:|-:|
|{{ stats.username }}|{{ stats.season }}|{{ stats.rank }}|{{ stats.wins }}|{{ stats.hotdogs }}|{{ ((stats.win_pct or 0) * 100)|round(1) }}%|{{ (stats.hotdogs_per_win or 0.0)|round(1) }}|
