import os
from concurrent.futures import ThreadPoolExecutor
from logging import ERROR, FATAL, Formatter, Handler, LogRecord

import httpx


class SlackFormatter(Formatter):
    def __init__(self) -> None:
        super().__init__(
            "`%(asctime)s : %(name)s : %(lineno)s : %(levelname)s` %(message)s"
        )

    def formatException(self, exc_info) -> str:
        msg = super().formatException(exc_info)
        return f"```\n{msg}\n```"

    def format(self, record: LogRecord) -> str:
        if record.exc_info:
            record.exc_text = self.formatException(record.exc_info)
        return super().format(record)


class SlackHandler(Handler):
    def __init__(self, *args, **kwargs) -> None:
        self.pool = ThreadPoolExecutor()
        super().__init__(*args, **kwargs)

    def close(self) -> None:
        self.pool.shutdown()
        return super().close()

    def emit(self, record: LogRecord) -> None:
        if "GDTBOT_LOG_SLACK_WEBHOOK" not in os.environ:
            return
        if record.levelno not in (FATAL, ERROR):
            return

        message = self.format(record)
        self.pool.submit(
            httpx.post,
            os.environ["GDTBOT_LOG_SLACK_WEBHOOK"],
            json={"text": message},
        )
