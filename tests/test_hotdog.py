import datetime as dt

import pytest

from gdtbot.lib.hotdog import db, model


def test_escape_markdown():
    assert db._escape_markdown("__ExampleUser__") == r"\\_\\_ExampleUser\\_\\_"
    assert db._escape_markdown("*_ExampleUser_*") == r"\\*\\_ExampleUser\\_\\*"
    assert db._escape_markdown("**ExampleUser__") == r"\\*\\*ExampleUser\\_\\_"
    assert db._escape_markdown("ExampleUser") == "ExampleUser"


@pytest.mark.asyncio
async def test_leaderboards():
    subreddit = "__testsubreddit__"
    game_pk = 123456

    game = model.Game(subreddit, game_pk, "commentid", 2024, dt.datetime.now())
    await db.write_game(game)

    play = model.Play("testuser", subreddit, game_pk, 654321, 1, None)
    await db.write_play(play)

    result = await db.get_recent_leaderboard(2024, subreddit)
    assert isinstance(result[0], model.SeasonLeaderboardRow)

    result = await db.get_season_leaderboard(2024, subreddit)
    assert isinstance(result[0], model.SeasonLeaderboardRow)
