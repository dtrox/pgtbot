import unittest

from gdtbot.lib.daily_thread import _read_discussion_questions


class TestQuestions(unittest.TestCase):
    def test(self):
        questions = _read_discussion_questions()
        self.assertIsInstance(questions, tuple)
        for q in questions:
            self.assertIsInstance(q, str)
