import unittest

from gdtbot.clients.wikipedia import Wikipedia


class TestRandomInCategory(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        wiki = Wikipedia()
        result = await wiki.random_in_category("Los_Angeles_Dodgers")
        self.assertIsInstance(result, tuple)
        self.assertIsInstance(result[0], str)
        self.assertTrue(result[0].startswith("https://"))
        self.assertIsInstance(result[1], str)
        await wiki.close()


if __name__ == "__main__":
    unittest.main()
