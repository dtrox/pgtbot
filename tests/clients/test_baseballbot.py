import unittest

from gdtbot.clients import baseballbot


class TestGameThreads(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        client = baseballbot.BaseballBotClient()

        result = await client.get_subreddit_game_threads("dodgers")
        self.assertIsInstance(result, list)
        self.assertIsInstance(result[0], dict)
        self.assertEqual(len(result), 25)

        await client.close()

    async def test_recent_subreddit_game_threads(self):
        client = baseballbot.BaseballBotClient()
        game_threads = await client.recent_subreddit_game_threads("dodgers")
        self.assertIsInstance(game_threads, list)
        self.assertIsInstance(game_threads[0], baseballbot.GameThread)
        await client.close()


if __name__ == "__main__":
    unittest.main()
