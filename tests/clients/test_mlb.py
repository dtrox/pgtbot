import datetime
import unittest

from gdtbot.clients import mlb


class TestSchedule(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        client = mlb.StatsAPIClient()
        schedule = await client.get(
            "/api/v1/schedule", sportId=1, teamId=119, date="2019-07-02"
        )
        self.assertIsInstance(schedule, dict)
        self.assertEqual(len(schedule["dates"]), 1)
        self.assertEqual(len(schedule["dates"][0]["games"]), 1)
        self.assertEqual(schedule["dates"][0]["games"][0]["gamePk"], 565842)

        games = await client.world_series(2018)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].teams.home, mlb.Team)
        self.assertIsInstance(games[0].teams.away, mlb.Team)
        self.assertEqual(len(games), 5)

        games = await client.spring_training(2019, 119)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].teams.home, mlb.Team)
        self.assertIsInstance(games[0].teams.away, mlb.Team)
        self.assertEqual(len(games), 32)

        games = await client.spring_training(2020, 119)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].teams.home, mlb.Team)
        self.assertIsInstance(games[0].teams.away, mlb.Team)
        self.assertEqual(len(games), 20)

        games = await client.games(datetime.date(2019, 4, 12), 119)
        schedule = await client.get(
            "/api/v1/schedule", date="2019-04-12", sportId=1, teamId=119
        )
        self.assertEqual(len(schedule["dates"][0]["games"]), len(games))
        for game in games:
            self.assertIsInstance(game, mlb.Game)
            self.assertTrue(game.teams.home.id == 119 or game.teams.away.id == 119)
            self.assertIsInstance(game.teams.away.probablePitcher, int)

        await client.close()


class TestTeam(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        client = mlb.StatsAPIClient()
        team = await client.team(119)
        self.assertIsInstance(team, mlb.Team)
        self.assertEqual(team.id, 119)

        await client.close()


class TestPeople(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        client = mlb.StatsAPIClient()
        pitcher, _, _ = await client.player(477132)
        self.assertIsInstance(pitcher, mlb.Player)
        self.assertEqual(pitcher.fullName, "Clayton Kershaw")
        self.assertEqual(pitcher.pitchHand, "L")

        batter, _, _ = await client.player(605141)
        self.assertIsInstance(batter, mlb.Player)
        self.assertEqual(batter.fullName, "Mookie Betts")
        self.assertEqual(batter.batSide, "R")

        await client.close()


class TestSeasonDates(unittest.IsolatedAsyncioTestCase):
    async def test(self):
        client = mlb.StatsAPIClient()
        season = await client.season(datetime.date(2019, 2, 28))
        self.assertEqual(season, 2019)
        season = await client.season(datetime.date(2019, 7, 4))
        self.assertEqual(season, 2019)
        season = await client.season(datetime.date(2019, 10, 15))
        self.assertEqual(season, 2019)
        season = await client.season(datetime.date(2019, 11, 1))
        self.assertEqual(season, 2020)

        day_type = await client.day_type(datetime.date(2019, 1, 1))
        self.assertEqual(day_type, "O")
        day_type = await client.day_type(datetime.date(2019, 2, 28))
        self.assertEqual(day_type, "S")
        day_type = await client.day_type(datetime.date(2019, 7, 4))
        self.assertEqual(day_type, "R")
        day_type = await client.day_type(datetime.date(2019, 10, 15))
        self.assertEqual(day_type, "P")
        day_type = await client.day_type(datetime.date(2019, 11, 1))
        self.assertEqual(day_type, "O")
        day_type = await client.day_type(datetime.date(2020, 2, 2))
        self.assertEqual(day_type, "O")

        await client.close()


if __name__ == "__main__":
    unittest.main()
