import re
import unittest

from gdtbot.lib import automod

_EXAMPLE_ENABLED = """
---

# These rules are enabled at the start of a game and disabled 30 minutes after it ends.
# GDT_BOT: BEGIN test RULES

    type: submission
    action: remove

# GDT_BOT: END test RULES

---
"""


_EXAMPLE_ENABLED_RULE_ONLY = """# GDT_BOT: BEGIN test RULES

    type: submission
    action: remove

# GDT_BOT: END test RULES"""


_EXAMPLE_DISABLED = """
---

# These rules are enabled at the start of a game and disabled 30 minutes after it ends.
# GDT_BOT: BEGIN test RULES
#
#    type: submission
#    action: remove
#
# GDT_BOT: END test RULES

---
"""


_EXAMPLE_DISABLED_RULE_ONLY = """# GDT_BOT: BEGIN test RULES
#
#    type: submission
#    action: remove
#
# GDT_BOT: END test RULES"""


class TestAutomod(unittest.TestCase):
    def test_validate(self):
        automod._validate_yaml("valid: true")
        try:
            automod._validate_yaml(
                "test: value\nnot anything\nwhat is love: baby don't hurt me\n"
            )
            self.fail()
        except automod.AutomodConfigInvalidError:
            pass

    def test_uncomment_repl(self):
        pat = re.compile(
            automod._RULE_GROUP_REGEX_TEMPLATE.format(rule_group="test"), re.MULTILINE
        )
        match = pat.search(_EXAMPLE_DISABLED_RULE_ONLY)
        assert match is not None
        result = automod._uncomment_repl(match)
        self.assertEqual(result, _EXAMPLE_ENABLED_RULE_ONLY)

    def test_enable(self):
        result = automod.enable("test", _EXAMPLE_DISABLED)
        self.assertEqual(result, _EXAMPLE_ENABLED)

    def test_comment_repl(self):
        pat = re.compile(
            automod._RULE_GROUP_REGEX_TEMPLATE.format(rule_group="test"), re.MULTILINE
        )
        match = pat.search(_EXAMPLE_ENABLED_RULE_ONLY)
        assert match is not None
        result = automod._comment_repl(match)
        self.assertEqual(result, _EXAMPLE_DISABLED_RULE_ONLY)

    def test_disable(self):
        result = automod.disable("test", _EXAMPLE_ENABLED)
        self.assertEqual(result, _EXAMPLE_DISABLED)


if __name__ == "__main__":
    unittest.main()
