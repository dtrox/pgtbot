import logging

from gdtbot.logging import SlackHandler, SlackFormatter


root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(
    logging.Formatter("%(asctime)s : %(name)s : %(levelname)s : %(message)s")
)
root_logger.addHandler(stream_handler)
# slack_handler = SlackHandler()
# slack_handler.setLevel(logging.ERROR)
# slack_handler.setFormatter(SlackFormatter())
# root_logger.addHandler(slack_handler)
other_stream_handler = logging.StreamHandler()
other_stream_handler.setLevel(logging.ERROR)
other_stream_handler.setFormatter(
    logging.Formatter("%(asctime)s : %(name)s : %(levelname)s : %(message)s")
)
root_logger.addHandler(other_stream_handler)


logger = logging.getLogger(__name__)
logger.info("This is an info log.")
logger.error("This is an error log.")
try:
    raise ValueError("This is an exception.")
except:
    logger.error("This is an error log with exc_info=True log.", exc_info=True)
